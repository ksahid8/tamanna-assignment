import React from "react";
import { GlobalStateProvider } from "./context-api";
import { ReactNotifications } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import Weather from "containers/Weather";
import "styles/App.css";

const App = () => {
  return (
    <GlobalStateProvider>
      <ReactNotifications />
      <Weather />
    </GlobalStateProvider>
  );
};

export default App;
