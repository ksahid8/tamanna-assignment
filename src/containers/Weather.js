import React from "react";
import WeatherDetail from "./content";
import BackgroundImage from "images/bg.webp";
import Sidebar from "./sidebar";

const Weather = () => {
  return (
    <div
      style={{ backgroundImage: `url(${BackgroundImage})` }}
      className="m-0 p-0 full-bg"
    >
      <div className="flex m-10 gap-4 ">
        <div className="w-9/12 p-4 rounded-lg shadow-lg bg-white">
          <WeatherDetail />
        </div>
        <div className="w-3/12 p-4 rounded-lg shadow-lg bg-white">
          <Sidebar />
        </div>
      </div>
    </div>
  );
};

export default Weather;
