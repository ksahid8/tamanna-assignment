import CitySkeleton from "components/skeletons/CitySkeleton";
import React, { useContext, useState } from "react";
import { GlobalState } from "context-api";
import { AiOutlinePlus, AiOutlineSearch, AiOutlineClose } from "react-icons/ai";
import { Store } from "react-notifications-component";
import { alertOptions } from "utils/constants";

const Add = () => {
  const {
    FetchDropDownCities,
    dropdownCitiesLoading,
    dropdownCities,
    selectedCities,
    setSelectedCities,
    StoreCities,
  } = useContext(GlobalState);
  const [query, setQuery] = useState("");

  const DropDownCard = ({ item }) => {
    let isSelected = false;
    const result = selectedCities.map((a) => {
      if (a.lat == item.lat && a.lon == item.lon) {
        isSelected = true;
      }
    });

    return (
      <div className="flex  justify-between py-1 cursor-pointer">
        <div className="flex justify-between w-5/6">
          <div className="text-sm">
            {item.name}
            {item.state ? ", " + item.state : ""}, {item.country}
          </div>
          <div className="text-xs text-[#888888]">
            {item.lat}, {item.lon}
          </div>
        </div>
        {!isSelected ? (
          <div
            className="flex gap-2 w-1/6 text-xs text-[#666666] font-normal bg-[#f4f5f7]  py-0.5 px-4 rounded-lg w-auto  items-center cursor-pointer hover:bg-[#df651e] hover:text-[#ffffff]"
            onClick={() => {
              const currentCities =
                JSON.parse(localStorage.getItem("selectedCities")) || [];
              {
                setSelectedCities([
                  ...selectedCities,
                  {
                    name: item?.name,
                    state: item?.state,
                    country: item?.country,
                    lat: item?.lat,
                    lon: item?.lon,
                  },
                ]);

                StoreCities([
                  ...currentCities,
                  {
                    name: item?.name,
                    state: item?.state,
                    country: item?.country,
                    lat: item?.lat,
                    lon: item?.lon,
                  },
                ]);
              }
              Store.addNotification({
                title: "Success!",
                message: "City Added Successfully",
                type: "success",
                ...alertOptions,
              });
            }}
          >
            <AiOutlinePlus /> Add
          </div>
        ) : (
          <div
            className="flex gap-2 w-1/6 text-xs text-[#666666] font-normal bg-[#f4f5f7]  py-0.5 px-1 rounded-lg w-auto  items-center cursor-pointer hover:bg-[#df651e] hover:text-[#ffffff]"
            onClick={() => {
              {
                setSelectedCities(
                  selectedCities.filter(
                    (a) => a?.lat !== item?.lat && a?.lon !== item?.lon
                  )
                );
                localStorage.setItem(
                  "selectedCities",
                  JSON.stringify(
                    selectedCities.filter(
                      (a) => a?.lat !== item?.lat && a?.lon !== item?.lon
                    )
                  )
                );
                Store.addNotification({
                  title: "Success!",
                  message: "City Removed Successfully",
                  type: "success",
                  ...alertOptions,
                });
              }
            }}
          >
            <AiOutlineClose /> Remove
          </div>
        )}
      </div>
    );
  };

  return (
    <div className="p-4 flex flex-col">
      <div className="flex flex-col gap-1">
        <div className="flex gap-2">
          <input
            onChange={(e) => setQuery(e.target.value)}
            value={query}
            placeholder="Search city"
            className="h-8 w-5/6 rounded-lg bg-[#f4f5f7] pl-6"
          />
          <div
            onClick={() => FetchDropDownCities(query)}
            className="w-1/6 text-sm text-white font-normal bg-[#f07c39] px-4 py-1 rounded-lg flex gap-1  items-center cursor-pointer hover:bg-[#df651e]"
          >
            <AiOutlineSearch /> <span>Search</span>
          </div>
        </div>
      </div>
      {dropdownCities?.length > 0 ? (
        <div className="flex flex-col shadow-md py-2 px-2">
          {dropdownCities.map((item, index) => (
            <DropDownCard key={index} item={item} />
          ))}
        </div>
      ) : dropdownCitiesLoading ? (
        <div className="flex flex-col gap-2 mt-2">
          {[...Array(5).keys()].map((item, index) => (
            <CitySkeleton key={index} />
          ))}
        </div>
      ) : (
        <div className="text-sm text-[#888888] text-center mt-6 h-20">
          No cities found!
        </div>
      )}
    </div>
  );
};

export default Add;
