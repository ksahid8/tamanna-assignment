import React, { useContext, useState } from "react";
import { GlobalState } from "context-api";
import CitySkeleton from "components/skeletons/CitySkeleton";
import { AiOutlinePlus, AiOutlineClose } from "react-icons/ai";
import Add from "./Add";
import CustomModal from "components/CustomModal";
import { Store } from "react-notifications-component";
import { alertOptions } from "utils/constants";

const Sidebar = () => {
  const {
    selectedCities,
    setSelectedCities,
    citiesLoading,
    setDropdownCities,
    setRegion,
  } = useContext(GlobalState);
  const [showaddModal, setShowAddModal] = useState(false);
  return (
    <div className="flex flex-col gap-2.5">
      <CustomModal
        modalHeading="Add City"
        isOpen={showaddModal}
        closeModal={() => {
          setShowAddModal(false);
          setTimeout(() => {
            setDropdownCities([]);
          }, 1000);
        }}
      >
        <Add />
      </CustomModal>
      <div className="flex justify-between items-center">
        <div className="text-md font-semibold">Cities</div>
        <div
          className="text-sm text-white font-normal bg-[#f07c39] px-4 py-1 rounded-lg flex gap-1  items-center cursor-pointer hover:bg-[#df651e]"
          onClick={() => setShowAddModal(true)}
        >
          <AiOutlinePlus /> <span>Add City</span>
        </div>
      </div>
      {selectedCities?.length > 0 ? (
        selectedCities.map((item, index) => (
          <div
            className="flex  items-center bg-[#f4f5f7] rounded-lg"
            key={index}
          >
            <div
              onClick={() => {
                setRegion({
                  lat: item?.lat,
                  lon: item?.lon,
                  city: item?.name,
                  country: item?.country,
                });
              }}
              key={index}
              className="w-5/6 px-4 text-sm py-2 cursor-pointer hover:text-[#f07c39] truncate"
            >
              {item.name}, {item.country}
            </div>
            <div
              className="w-1/6  flex gap-2 w-1/6 text-xs h-6 text-[#666666] font-normal   py-0.5 px-1  w-auto  items-center cursor-pointer  hover:text-[#df651e]"
              onClick={() => {
                {
                  setSelectedCities(
                    selectedCities.filter(
                      (a) => a?.lat !== item?.lat && a?.lon !== item?.lon
                    )
                  );
                  Store.addNotification({
                    title: "Success!",
                    message: "City Removed Successfully",
                    type: "success",
                    ...alertOptions,
                  });
                }
              }}
            >
              <AiOutlineClose />
            </div>
          </div>
        ))
      ) : citiesLoading ? (
        [...Array(8).keys()].map((item, index) => (
          <CitySkeleton key={index}>{item}</CitySkeleton>
        ))
      ) : (
        <div className="text-lg text-[#888888] text-center mt-6">
          No cities found!
        </div>
      )}
    </div>
  );
};

export default Sidebar;
