import React, { useContext, useEffect, useState } from "react";
import { GlobalState } from "context-api";
import {
  getLocalDayTime,
  getWeatherIcon,
  getWeatherTemp,
  getWeatherWind,
} from "utils/constants";
import CurrentSkeleton from "components/skeletons/CurrentSkeleton";
import { Store } from "react-notifications-component";
import { alertOptions } from "utils/constants";

const Current = () => {
  const { isCelsius, setIsCelsius, weatherData, weatherLoading, region } =
    useContext(GlobalState);

  const [currentValues, setCurrentValues] = useState({
    icon: "",
    temp: "",
    hum: "",
    wind: "",
    date: "",
    desc: "",
    press: "",
    dew: "",
    vis: "",
  });

  useEffect(() => {
    if (weatherData?.lat && weatherData?.lat) {
      setCurrentValues({
        ...currentValues,
        icon: getWeatherIcon(weatherData?.current.weather[0].icon),
        temp: Math.round(weatherData?.current?.temp),
        hum: weatherData?.current?.humidity,
        wind: weatherData?.current?.wind_speed.toFixed(2),
        date: getLocalDayTime(weatherData?.current?.dt, weatherData?.timezone),
        desc: weatherData?.current.weather[0].description,
        press: weatherData?.current?.pressure,
        dew: weatherData?.current?.dew_point.toFixed(2),
        vis: (weatherData?.current?.visibility / 1000).toFixed(2),
      });
    }
  }, [weatherData]);

  return weatherLoading ? (
    <CurrentSkeleton />
  ) : (
    <div className="flex items-center justify-between px-2">
      <div className="flex items-center gap-2.5">
        <div>
          <img src={currentValues?.icon} alt="weather-icon" />
        </div>
        <div className="text-7xl">{currentValues?.temp}</div>
        <div className="flex text-lg  self-start mt-4">
          <div
            className={` mr-2 cursor-pointer ${
              isCelsius ? "text-[#000000]" : "text-[#888888]"
            }`}
            onClick={() => {
              setIsCelsius(true);
              Store.addNotification({
                title: "Success!",
                message: "Unit Changed To Celcius",
                type: "success",
                ...alertOptions,
              });
            }}
          >
            °C
          </div>
          <div
            className={` border-l pl-2 cursor-pointer ${
              !isCelsius ? "text-[#000000]" : "text-[#888888]"
            }`}
            onClick={() => {
              setIsCelsius(false);
              Store.addNotification({
                title: "Success!",
                message: "Unit Changed To Fahrenheit",
                type: "success",
                ...alertOptions,
              });
            }}
          >
            °F
          </div>
        </div>
        <div className="flex flex-col text-xs text-[#666666]">
          <div>Humidity : {currentValues?.hum}%</div>
          <div>Wind : {getWeatherWind(currentValues?.wind, isCelsius)}</div>
          <div>Pressure : {currentValues?.press}hPa</div>
        </div>
        <div className="flex flex-col text-xs text-[#666666]">
          <div>Dew point : {getWeatherTemp(currentValues?.dew, isCelsius)}</div>
          <div>Visibility : {currentValues?.vis}km</div>
        </div>
      </div>
      <div className="flex flex-col items-end">
        <div className="text-xl capitalize">
          {region.city}
          {region.state ? ", " + region.state : ""}, {region.country}
        </div>
        <div className="text-[#666666] text-sm">{currentValues?.date}</div>
        <div className="text-[#f07c39] text-lg capitalize">
          {currentValues?.desc}
        </div>
      </div>
    </div>
  );
};

export default Current;
