import React, { useContext, useEffect, useState } from "react";
import { GlobalState } from "context-api";
import { Bar } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  BarElement,
} from "chart.js";
import ChartDataLabels from "chartjs-plugin-datalabels";
import { getLocalTimeShort } from "utils/constants";

const Wind = () => {
  const { weatherData, isCelsius } = useContext(GlobalState);

  const [chartLabels, setChartLabels] = useState([]);
  const [chartData, setChartData] = useState([]);
  const [minMax, setMinMax] = useState({ min: 0, max: 100 });

  useEffect(() => {
    if (weatherData?.hourly && weatherData?.hourly?.length > 0) {
      const allData = weatherData?.hourly;
      const labels = allData.map((item) => {
        return getLocalTimeShort(item.dt, weatherData?.timezone);
      });
      setChartLabels(labels.slice(0, 8));
      const winds = allData.map((item) => {
        return item?.wind_speed;
      });
      setChartData(winds.slice(0, 8));
      setMinMax({
        min: 0,
        max: winds.slice(0, 8)[7] * 2,
      });
    }
  }, [weatherData]);

  ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    BarElement,
    ChartDataLabels
  );

  const options = {
    elements: {
      line: {
        tension: 0,
      },
    },
    scales: {
      x: {
        grid: {
          display: false,
        },
      },
      y: {
        display: false,
        grid: {
          display: false,
        },
        ticks: {
          display: false,
        },
        min: minMax?.min,
        max: minMax?.max,
      },
    },
    responsive: true,
    plugins: {
      datalabels: {
        color: "#70a13d",
        offset: "top",
        backgroundColor: "#fff",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#c5dcad",
        formatter: (a) => {
          return isCelsius ? a + " m/s" : a + " mph";
        },
      },
      tooltip: {
        enabled: false,
      },
      legend: {
        display: false,
      },
    },
  };

  const data = {
    labels: chartLabels,
    datasets: [
      {
        label: "First dataset",
        data: chartData,
        fill: false,
        backgroundColor: "#ddeacf",
        borderColor: "#c5dcad",
        borderWidth: 1,
        lineTension: 0.5,
      },
    ],
  };

  return (
    <div className="p-2">
      <Bar options={options} data={data} height={50} />
    </div>
  );
};

export default Wind;
