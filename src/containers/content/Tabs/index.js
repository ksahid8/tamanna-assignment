import React, { useContext } from "react";
import { GlobalState } from "context-api";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import Temperature from "./Temperature";
import Wind from "./Wind";
import ChartSkeleton from "components/skeletons/ChartSkeleton";

const WeatherTabs = () => {
  const { weatherLoading } = useContext(GlobalState);

  return (
    <Tabs className="weather-tabs">
      <TabList>
        <Tab>Temperature</Tab>
        <Tab>Wind</Tab>
      </TabList>

      <TabPanel>
        {weatherLoading ? <ChartSkeleton /> : <Temperature />}
      </TabPanel>
      <TabPanel>{weatherLoading ? <ChartSkeleton /> : <Wind />}</TabPanel>
    </Tabs>
  );
};

export default WeatherTabs;
