import React, { useContext, useEffect, useState } from "react";
import { GlobalState } from "context-api";
import { Line } from "react-chartjs-2";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Filler,
} from "chart.js";
import ChartDataLabels from "chartjs-plugin-datalabels";
import { getLocalTimeShort, getWeatherTemp } from "utils/constants";

const Temperature = () => {
  const { weatherData, isCelsius } = useContext(GlobalState);

  const [chartLabels, setChartLabels] = useState([]);
  const [chartData, setChartData] = useState([]);

  const [minMax, setMinMax] = useState({ min: 0, max: 100 });

  useEffect(() => {
    if (weatherData?.hourly && weatherData?.hourly?.length > 0) {
      const allData = weatherData?.hourly;
      const labels = allData.map((item) => {
        return getLocalTimeShort(item.dt, weatherData?.timezone);
      });
      setChartLabels(labels.slice(0, 8));
      const temps = allData.map((item) => {
        return Math.round(item.temp);
      });
      setChartData(temps.slice(0, 8));
      setMinMax({
        min: 0,
        max: temps.slice(0, 8)[7] * 2,
      });
    }
  }, [weatherData]);

  ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Filler,
    ChartDataLabels
  );

  const options = {
    elements: {
      line: {
        tension: 0,
      },
    },
    scales: {
      x: {
        grid: {
          display: false,
        },
      },
      y: {
        display: false,
        grid: {
          display: false,
        },
        ticks: {
          display: false,
        },
        min: minMax?.min,
        max: minMax?.max,
      },
    },
    responsive: true,
    plugins: {
      datalabels: {
        color: "#f07c39",
        offset: "top",
        backgroundColor: "#fff",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#fbdece",
        formatter: (a) => {
          return a;
        },
      },
      tooltip: {
        enabled: false,
      },
      legend: {
        display: false,
      },
    },
  };

  const data = {
    labels: chartLabels,
    datasets: [
      {
        label: "First dataset",
        data: chartData,
        fill: true,
        backgroundColor: "#ffe4d5",
        borderColor: "#fbdece",
        borderWidth: 1,
        lineTension: 0.5,
      },
    ],
  };

  return (
    <div className="p-2">
      <Line options={options} data={data} height={50} />
    </div>
  );
};

export default Temperature;
