import React, { useState } from "react";
import Current from "./Current";
import DayDetail from "./DayDetail";
import Days from "./Days";
import WeatherTabs from "./Tabs";
import CustomModal from "components/CustomModal";

const WeatherDetail = () => {
  const [showDayDetail, setShowDayDetail] = useState(false);
  const [dayDetailData, setDayDetailData] = useState("");
  const [modalHeading, setModalHeading] = useState("");
  return (
    <div className="flex flex-col">
      <CustomModal
        modalHeading={modalHeading}
        isOpen={showDayDetail}
        closeModal={() => {
          setShowDayDetail(false);
        }}
      >
        <DayDetail dayDetailData={dayDetailData} />
      </CustomModal>
      <Current />
      <WeatherTabs />
      <Days
        setModalHeading={setModalHeading}
        setShowDayDetail={setShowDayDetail}
        setDayDetailData={setDayDetailData}
      />
    </div>
  );
};

export default WeatherDetail;
