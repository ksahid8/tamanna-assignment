import React, { useContext, useEffect, useState } from "react";
import { GlobalState } from "context-api";
import {
  getLocalTimeShort,
  getWeatherIcon,
  getWeatherTemp,
  getWeatherWind,
} from "utils/constants";
import { TbCloudRain } from "react-icons/tb";
import { RiWindyFill } from "react-icons/ri";
import { AiOutlineCompress } from "react-icons/ai";
import { WiHumidity, WiSunrise, WiSunset } from "react-icons/wi";
import { GiDew, GiExplosionRays } from "react-icons/gi";

const DayDetail = ({ dayDetailData }) => {
  const { isCelsius, weatherData } = useContext(GlobalState);

  const FlexItem = ({ icon, title, value }) => {
    return (
      <div className="flex gap-1 min-w-[45%] ">
        {icon}
        <span className="text-xs text-[#888888]">{title}:</span>
        <span className="text-xs text-[#888888]">{value}</span>
      </div>
    );
  };

  return (
    <div className="p-4 flex flex-col gap-4">
      <div className="flex gap-2">
        <div className="w-1/4">
          <img
            src={getWeatherIcon(dayDetailData?.weather[0]?.icon)}
            alt="weather-icon"
          />
        </div>
        <div className="w-3/4 flex flex-col gap-2">
          <div className="capitalize text-lg font-medium">
            {dayDetailData?.weather[0]?.description}
          </div>
          <div className="text-sm font-normal">{`The high will be ${getWeatherTemp(
            dayDetailData?.temp?.max,
            isCelsius
          )}, the low will be ${getWeatherTemp(
            dayDetailData?.temp?.min,
            isCelsius
          )}`}</div>
          <div className="flex gap-4 flex-wrap">
            <FlexItem
              icon={<TbCloudRain size={16} color="#f07c39" />}
              value={
                dayDetailData?.rain + "mm (" + dayDetailData?.clouds + "%)"
              }
              title="Rain"
            />
            <FlexItem
              icon={<RiWindyFill size={16} color="#f07c39" />}
              value={`${getWeatherWind(dayDetailData?.wind_speed, isCelsius)}`}
              title="Wind"
            />
            <FlexItem
              icon={<AiOutlineCompress size={16} color="#f07c39" />}
              value={`${dayDetailData?.pressure}hPa`}
              title="Pressure"
            />
            <FlexItem
              icon={<WiHumidity size={16} color="#f07c39" />}
              value={`${dayDetailData?.humidity}%`}
              title="Humidity"
            />
            <FlexItem
              icon={<GiExplosionRays size={16} color="#f07c39" />}
              value={`${dayDetailData?.uvi}`}
              title="UV"
            />
            <FlexItem
              icon={<GiDew size={16} color="#f07c39" />}
              value={`${getWeatherTemp(dayDetailData?.dew_point, isCelsius)}`}
              title="Dew Point"
            />
          </div>
        </div>
      </div>
      <div className="">
        <table className="temp-table">
          <tbody>
            <tr>
              <th></th>
              <th className="font-semibold">Morning</th>
              <th className="font-semibold">Afternoon</th>
              <th className="font-semibold">Evening</th>
              <th className="font-semibold">Night</th>
            </tr>
            <tr>
              <th className="text-[#888888]">Temperature</th>
              <th>{`${getWeatherTemp(
                dayDetailData?.temp?.morn,
                isCelsius
              )}`}</th>
              <th>{`${getWeatherTemp(
                dayDetailData?.temp?.day,
                isCelsius
              )}`}</th>
              <th>{`${getWeatherTemp(
                dayDetailData?.temp?.eve,
                isCelsius
              )}`}</th>
              <th>{`${getWeatherTemp(
                dayDetailData?.temp?.night,
                isCelsius
              )}`}</th>
            </tr>
            <tr>
              <th className="text-[#888888]">Feels Like</th>
              <th>{`${getWeatherTemp(
                dayDetailData?.feels_like?.morn,
                isCelsius
              )}`}</th>
              <th>{`${getWeatherTemp(
                dayDetailData?.feels_like?.day,
                isCelsius
              )}`}</th>
              <th>{`${getWeatherTemp(
                dayDetailData?.feels_like?.eve,
                isCelsius
              )}`}</th>
              <th>{`${getWeatherTemp(
                dayDetailData?.feels_like?.night,
                isCelsius
              )}`}</th>
            </tr>
          </tbody>
        </table>
      </div>
      <div className="flex">
        <FlexItem
          icon={<WiSunrise size={16} color="#f07c39" />}
          value={`${getLocalTimeShort(
            dayDetailData?.sunrise,
            weatherData?.timezone
          )}`}
          title="Sunrise"
        />
        <FlexItem
          icon={<WiSunset size={16} color="#f07c39" />}
          value={`${getLocalTimeShort(
            dayDetailData?.sunset,
            weatherData?.timezone
          )}`}
          title="Sunset"
        />
      </div>
    </div>
  );
};

export default DayDetail;
