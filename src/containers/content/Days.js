import DayCard from "components/DayCard";
import React, { useContext, useEffect, useState } from "react";
import { GlobalState } from "context-api";
import { getLocalDayShort, getWeatherIcon } from "utils/constants";
import DayCardSkeleton from "components/skeletons/DayCardSkeleton";

const Days = ({ setShowDayDetail, setDayDetailData, setModalHeading }) => {
  const { weatherData, weatherLoading, isCelsius } = useContext(GlobalState);

  const [dailyData, setDailyData] = useState([]);

  useEffect(() => {
    if (weatherData?.daily && weatherData?.daily?.length > 0) {
      const allData = weatherData?.daily;
      const filteredData = allData.map((item) => {
        return {
          dt: item?.dt,
          day: getLocalDayShort(item?.dt, weatherData?.timezone),
          icon: getWeatherIcon(item?.weather[0]?.icon),
          low: Math.round(item?.temp?.min) + "°",
          high: Math.round(item?.temp?.max) + "°",
          desc: item?.weather[0]?.description,
        };
      });
      setDailyData(filteredData);
    }
  }, [weatherData]);

  return (
    <div className="flex gap-1 overflow-scroll">
      {weatherLoading
        ? [...Array(8).keys()].map((item, index) => (
            <DayCardSkeleton key={index} />
          ))
        : dailyData.map((item, index) => (
            <DayCard
              timezone={weatherData?.timezone}
              setModalHeading={setModalHeading}
              item={item}
              key={index}
              setShowDayDetail={setShowDayDetail}
              setDayDetailData={setDayDetailData}
              dailyData={weatherData?.daily}
            />
          ))}
    </div>
  );
};

export default Days;
