import React, { createContext, useState, useEffect } from "react";
import { getWeatherDetails, getCities, getRegionData } from "utils/services";

export const GlobalState = createContext();

export const GlobalStateProvider = (props) => {
  const [isCelsius, setIsCelsius] = useState(true);
  const [weatherData, setweatherData] = useState({});
  const [weatherLoading, setWeatherLoading] = useState(false);
  const [citiesLoading, setCitiesLoading] = useState(false);
  const [dropdownCities, setDropdownCities] = useState([]);
  const [dropdownCitiesLoading, setDropdownCitiesLoading] = useState(false);
  const [selectedCities, setSelectedCities] = useState([]);
  const [region, setRegion] = useState({
    lat: "",
    lon: "",
    city: "",
    country: "",
  });

  const FetchWeatherDetails = async () => {
    setWeatherLoading(true);
    let payload = {
      lat: region?.lat,
      lon: region?.lon,
      isCelsius: isCelsius,
    };

    const response = await getWeatherDetails(payload);

    setweatherData(response);
    setWeatherLoading(false);
  };

  const FetchDropDownCities = async (query) => {
    setDropdownCitiesLoading(true);
    let payload = {
      query: query,
    };
    const response = await getCities(payload);
    setDropdownCities(response);
    setDropdownCitiesLoading(false);
  };

  const getLocation = () => {
    setWeatherLoading(true);
    if (!navigator.geolocation) {
      console.log("Geolocation is not supported by your browser");
      setRegion({
        lat: 38.7259284,
        lon: -9.137382,
        city: "Lisbon",
        country: "PT",
      });
      setWeatherLoading(false);
    } else {
      navigator.geolocation.getCurrentPosition(
        async (position) => {
          const response = await getRegionData(
            position?.coords?.latitude,
            position?.coords?.longitude
          );

          setRegion({
            lat: position.coords.latitude,
            lon: position.coords.longitude,
            city: response[0].name,
            country: response[0].country,
          });
        },
        () => {
          console.log("Unable to retrieve your location");
          setRegion({
            lat: 38.7259284,
            lon: -9.137382,
            city: "Lisbon",
            country: "PT",
          });
        }
      );
    }
  };

  useEffect(() => {
    if (region?.lat && region?.lon) {
      FetchWeatherDetails();
    }
  }, [isCelsius, region]);

  useEffect(() => {
    getLocation();
  }, []);

  const StoreCities = (cities) => {
    localStorage.setItem("selectedCities", JSON.stringify(cities));
  };

  useEffect(() => {
    if (localStorage.getItem("selectedCities")) {
      setSelectedCities(JSON.parse(localStorage.getItem("selectedCities")));
    } else {
      setSelectedCities([]);
    }
  }, [localStorage.getItem("selectedCities")]);

  return (
    <GlobalState.Provider
      value={{
        isCelsius,
        setIsCelsius,
        weatherLoading,
        setWeatherLoading,
        weatherData,
        citiesLoading,
        FetchDropDownCities,
        dropdownCitiesLoading,
        dropdownCities,
        setDropdownCities,
        selectedCities,
        setSelectedCities,
        region,
        setRegion,
        StoreCities,
      }}
    >
      {props.children}
    </GlobalState.Provider>
  );
};
