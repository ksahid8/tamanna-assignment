import { getData } from "./apiCall";
import { WEATHER_APP_ID } from "./constants";
import { weatherData, cityData } from "dummy";

export async function getWeatherDetails(payload) {
  const { lat = 9.9399, lon = 76.2602, isCelsius } = payload;

  let units;
  if (isCelsius) {
    units = "metric";
  } else {
    units = "imperial";
  }

  // return await weatherData;
  return await getData(
    `onecall?lat=${lat}&lon=${lon}&appid=${WEATHER_APP_ID}&units=${units}`
  ).then((response) => {
    if (response) {
      return response;
    }
  });
}

export async function getCities(payload) {
  const { query = "" } = payload;

  // return await cityData;
  return await getData(
    `http://api.openweathermap.org/geo/1.0/direct?q=${query}&appid=${WEATHER_APP_ID}&limit=${5}`
  ).then((response) => {
    if (response) {
      return response;
    }
  });
}

export async function getRegionData(lat, lon) {
  // return await cityData;
  return await getData(
    `http://api.openweathermap.org/geo/1.0/reverse?lat=${lat}&lon=${lon}&limit=1&appid=${WEATHER_APP_ID}`
  ).then((response) => {
    if (response) {
      return response;
    }
  });
}
