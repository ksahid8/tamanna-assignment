import axios from "axios";
const axiosApiInstance = axios.create({
  baseURL: "https://api.openweathermap.org/data/3.0/",
});

axiosApiInstance.interceptors.request.use(
  async (config) => {
    config.headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
    };
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

export default axiosApiInstance;
