import moment from "moment-timezone";

export const WEATHER_APP_ID = "615958060d58e914e2caf2debca16948";
export const getLocalDay = (integer, timezone) => {
  return moment(integer * 1000)
    .tz(timezone)
    .format("dddd");
};
export const getLocalTimeDay = (integer, timezone) => {
  return moment(integer * 1000)
    .tz(timezone)
    .format("MMM DD YYYY");
};
export const getLocalDayTime = (integer, timezone) => {
  return moment(integer * 1000)
    .tz(timezone)
    .format("MMM DD YYYY, h:mm A");
};
export const getLocalDayShort = (integer, timezone) => {
  return moment(integer * 1000)
    .tz(timezone)
    .format("ddd");
};
export const getLocalTimeShort = (integer, timezone) => {
  return moment(integer * 1000)
    .tz(timezone)
    .format("h:mm a");
};
export const getWeatherIcon = (icon) => {
  return `http://openweathermap.org/img/wn/${icon}@2x.png`;
};

export const getWeatherTemp = (temp, isCelsius) => {
  let symbol;

  if (isCelsius) {
    symbol = Math.round(temp) + "°C";
  } else {
    symbol = Math.round(temp) + "°F";
  }

  return symbol;
};

export const getWeatherWind = (wind, isCelsius) => {
  let symbol;

  if (isCelsius) {
    symbol = wind + " m/s";
  } else {
    symbol = wind + " mph";
  }

  return symbol;
};

export const alertOptions = {
  insert: "top",
  container: "bottom-left",
  animationIn: ["zoom-in"],
  animationOut: ["zoom-out"],
  dismiss: {
    duration: 3000,
  },
};
