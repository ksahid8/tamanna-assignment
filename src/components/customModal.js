import React from "react";
import Modal from "react-modal";
import { AiOutlineClose } from "react-icons/ai";
const CustomModal = ({ isOpen, children, closeModal, modalHeading }) => {
  const customStyles = {
    content: {
      zIndex: 999,
      width: 600,
      padding: 0,
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      border: "none",
      boxShadow: "#0000001c 1px 1px 13px",
    },
  };

  return (
    <Modal
      ariaHideApp={false}
      isOpen={isOpen}
      closeTimeoutMS={200}
      onRequestClose={closeModal}
      style={customStyles}
      contentLabel="Example Modal"
    >
      <div className="modal-header">
        <div className="title">{modalHeading}</div>
        <div className="close" onClick={closeModal}>
          <AiOutlineClose />
        </div>
      </div>
      {children}
    </Modal>
  );
};

export default CustomModal;
