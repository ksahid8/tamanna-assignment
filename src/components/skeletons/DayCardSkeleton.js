import React from "react";

const DayCardSkeleton = () => {
  return (
    <div className="rounded-lg flex flex-col p-4 mx-2 my-4 shadow-lg items-center cursor-pointer w-[12.5%] gap-2">
      <div className="animate-pulse bg-[#e5e5e6] h-6 w-full rounded-lg"></div>
      <div className="animate-pulse bg-[#e5e5e6] h-10 w-full rounded-lg"></div>
      <div className="animate-pulse bg-[#e5e5e6] h-6 w-full rounded-lg"></div>
      <div className="animate-pulse bg-[#e5e5e6] h-4 w-full rounded-lg"></div>
    </div>
  );
};

export default DayCardSkeleton;
