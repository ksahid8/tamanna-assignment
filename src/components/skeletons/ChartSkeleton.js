import React from "react";

const ChartSkeleton = () => {
  return (
    <div className="animate-pulse bg-[#e5e5e6] h-44 w-full rounded-lg"></div>
  );
};

export default ChartSkeleton;
