import React from "react";

const CurrentSkeleton = () => {
  return (
    <div className="flex items-center justify-between mb-2">
      <div className="flex items-center gap-2.5">
        <div className="animate-pulse bg-[#e5e5e6] h-20 w-20 rounded-lg"></div>
        <div className="flex animate-pulse bg-[#e5e5e6] h-20 w-32 rounded-lg"></div>
        <div className="flex flex-col text-xs text-[#666666] gap-2">
          <div className="animate-pulse bg-[#e5e5e6] h-5 w-32 rounded-lg"></div>
          <div className="animate-pulse bg-[#e5e5e6] h-5 w-32 rounded-lg"></div>
          <div className="animate-pulse bg-[#e5e5e6] h-5 w-32 rounded-lg"></div>
        </div>
        <div className="flex flex-col text-xs text-[#666666] gap-2">
          <div className="animate-pulse bg-[#e5e5e6] h-5 w-32 rounded-lg"></div>
          <div className="animate-pulse bg-[#e5e5e6] h-5 w-32 rounded-lg"></div>
          <div className="animate-pulse bg-[#e5e5e6] h-5 w-32 rounded-lg"></div>
        </div>
      </div>
      <div className="flex flex-col text-xs text-[#666666] gap-2">
        <div className="animate-pulse bg-[#e5e5e6] h-5 w-32 rounded-lg"></div>
        <div className="animate-pulse bg-[#e5e5e6] h-5 w-32 rounded-lg"></div>
        <div className="animate-pulse bg-[#e5e5e6] h-5 w-32 rounded-lg"></div>
      </div>
    </div>
  );
};

export default CurrentSkeleton;
