import React from "react";

const CitySkeleton = () => {
  return (
    <div className="animate-pulse bg-[#e5e5e6] h-6 w-full rounded-lg"></div>
  );
};

export default CitySkeleton;
