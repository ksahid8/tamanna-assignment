import React from "react";
import { getLocalTimeDay } from "utils/constants";

const DayCard = ({
  timezone,
  item,
  setShowDayDetail,
  setDayDetailData,
  dailyData,
  setModalHeading,
}) => {
  const getDailyData = (dt) => {
    setModalHeading(getLocalTimeDay(dt, timezone));
    const indexData = dailyData.filter((item) => item?.dt === dt)[0];
    setDayDetailData(indexData);
  };

  const { dt, day, icon, high, low, desc } = item;
  return (
    <div
      className="rounded-lg w-[12.5%] flex flex-col p-4 mx-2 my-4 shadow-lg items-center cursor-pointer transition ease-in-out delay-100  hover:-translate-y-1 hover:scale-15  duration-300 hover:shadow-[0px_3px_20px_#0000002b]"
      onClick={() => {
        setShowDayDetail(true);
        getDailyData(dt);
      }}
    >
      <div className="font-medium">{day}</div>
      <div>
        <img alt="day-weather-icon" src={icon} />
      </div>
      <div className="flex gap-1">
        <div className="text-sm ">{high}</div>
        <div className="text-sm text-[#888888]">{low}</div>
      </div>
      <div className="text-[10px] text-[#888888] text-center capitalize leading-3 mt-1">
        {desc}
      </div>
    </div>
  );
};

export default DayCard;
